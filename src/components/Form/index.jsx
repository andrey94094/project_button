import { PureComponent } from 'react';
import "./Form.scss"
import 'font-awesome/css/font-awesome.min.css';

class Form extends PureComponent {
    state = {
        isLoading: false,
        isShowForm: true,
        tel: '',
    };
    handleChangeTel = (e) => {
        const val = e.target.value;
        if (e.target.validity.valid) this.setState({ tel: e.target.value });
    };

    handleClick = async () => {
        this.setState({ isLoading: true });
        try {
            await fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                body: JSON.stringify({
                    tel: this.state.tel,
                }),
            })
            this.setState({ isLoading: false });
            this.setState({ isShowForm: false });
        } catch (err) {
        }
    }

    render() {
        const { isShowForm, isLoading, tel } = this.state;
        return (
            <section className="form">
                {isShowForm ?
                    (<div className="form__wrapper">
                        <input className="form__input" placeholder={"Ваш номер..."} value={this.state.tel} pattern="^-?[0-9]\d*$" onChange={this.handleChangeTel } name="tel"></input>
                        <button className="form__button" onClick={this.handleClick} disabled={tel.length < 7  || tel.length > 12 || isLoading}><div className="form__text">ЗАКАЗАТЬ</div></button>
                    </div>
                    ) : `мы с вами свяжемся по ${this.state.tel} телефону`}
                    <button class="fa fa-arrow-circle-left fa-spin"></button>
            </section>
        )
    }
}

export default Form;
