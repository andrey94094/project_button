import { PureComponent } from 'react';
import Form from '../Form';

class App extends PureComponent {
  render() {
    return (
      <main>
        <Form />
      </main>
    )
  }
}


export default App;
